# Map for cart in Shopify

This code will help you let customer set delivery coordinates in countries where shipping address is not perfect solution (UAE, Costa Rica). From Google Maps API it is possible to prefill shipping address (partly). Pass coordinates to Shopify order as **cart attribute**.

## What will you need? 
* Basic JavaScript skills
* Your own Google Maps API key (basic plan is FREE)
* Basic Shopify Dev knowhow

## Demo
Map only: https://amazing-ride-0aebaa.netlify.com/map.html
Live solution: https://lacarne.ae

## Custom implementation
Yes, sure. Feel free to reach me. You can find contacts at https://martinhalik.cz
I work for https://soundsgood.agency